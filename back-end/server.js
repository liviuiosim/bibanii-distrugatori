const express = require("express");
const bodyParser = require("body-parser");
const sequelize = require("sequelize");
const cors = require("cors");
const session = require("client-sessions");

const connection = new sequelize("bibaniiDB", "root", "", {
  dialect: "mysql"
}); //1st param = name, 2nd param = root, 3rd param = password, 4th param = optiuni extra

const app = express();
let port = 8081;

const configure = app => {
  app.use(cors());

  app.use(
    session({
      cookieName: "session",
      secret: "cod secret foarte secret",
      duration: 7200000,
      activeDuration: 300000,
      httpOnly: true,
      ephemeral: true
    })
  );
  app.use(bodyParser.json());

  //--------------------START MODELS

  const User = connection.define("user", {
    lastName: sequelize.STRING,
    firstName: sequelize.STRING,
    email: sequelize.STRING,
    password: sequelize.STRING,
    token: sequelize.STRING
  });

  const Group = connection.define("group", {
    name: sequelize.STRING
  });

  const UserGroup = connection.define("userGroup");

  const GroupNotes = connection.define("groupNotes");

  const Note = connection.define("note", {
    name: sequelize.STRING,
    date: sequelize.STRING,
    subject: sequelize.STRING,
    label: sequelize.STRING,
    content: sequelize.TEXT
  });

  User.hasMany(Group);
  User.hasMany(UserGroup);
  Group.hasMany(UserGroup);

  Note.hasMany(GroupNotes);
  Group.hasMany(GroupNotes);
  User.hasMany(Note);

  //--------------------END MODELS

  const checkLogin = async (req, res, next) => {
    const { token, id } = req.session;

    if (!token || !id) {
      res.status(403).send({ message: "Forbidden" });
    } else {
      const user = await User.findOne({ where: { token, id }, raw: true });

      if (!user) {
        res.status(403).send({ message: "Forbidden" });
      } else {
        next();
      }
    }
  };

  app.post("/api/login", async (req, res) => {
    const { email, password } = req.body;

    const user = await User.findOne({ where: { email, password }, raw: true });

    if (!user) {
      res.status(403).send({ message: "Incorrect email or password" });
    } else {
      if (req.session.id) {
        res.status(202).send({ message: "Already logged it" });
      } else {
        req.session.id = user.id;
        req.session.token = user.token;
        res.status(200).send({ message: "Successful login" });
      }
    }
  });

  app.post("/api/register", async (req, res) => {
    try {
      const { firstName, lastName, email, password } = req.body;

      const errors = [];

      if (!firstName) {
        errors.push("First name is empty");
      }
      if (!lastName) {
        errors.push("Last name is empty");
      }
      if (!email) {
        errors.push("Email is empty");
      } else if (
        !email.match(
          /(?:[a-z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@stud.ase.ro/
        )
      ) {
        errors.push("Email is not valid");
      } else if (await User.findOne({ where: { email }, raw: true })) {
        errors.push("Email already used");
      }
      if (!password) {
        errors.push("Password is empty");
      }

      if (errors.length === 0) {
        await User.create({
          firstName,
          lastName,
          email,
          password,
          token: Math.random().toString(36)
        });
        res.status(201).send({
          message: `User ${firstName} ${lastName} was sucessfull created`
        });
      } else {
        res.status(400).send({ errors });
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  });

  app.get("/api/logout", checkLogin, async (req, res) => {
    req.session.reset();
    res.status(200).send({ message: "Successful logout" });
  });

  app.get("/api/reset", async (req, res) => {
    try {
      await connection.sync({
        force: true
      });
      res.status(200).send({ message: "bd reseted succes" });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.get("/api/notes", checkLogin, async (req, res) => {
    try {
      const notes = await Note.findAll({
        where: { userId: req.session.id },
        raw: true
      });
      res.status(200).send(notes);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.post("/api/notes", checkLogin, async (req, res) => {
    try {
      const { name, subject, label, content, date } = req.body;

      await Note.create({
        name,
        subject,
        label,
        date,
        content,
        userId: req.session.id
      });
      res.status(200).send({ message: "note inserted" });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.get("/api/notes/:id", checkLogin, async (req, res) => {
    try {
      const { id } = req.params;
      const foundNote = await Note.findOne({
        where: {
          id //id : id
        }
      });
      if (!foundNote) {
        res.status(400).send({ message: "note does not exist" });
      } else {
        res.status(200).send(foundNote);
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.put("/api/notes/:id", checkLogin, async (req, res) => {
    try {
      const { id } = req.params;
      let { name, subject, label, content, date } = req.body;

      const foundNote = await Note.findOne({
        where: {
          id,
          userId: req.session.id //id : id
        }
      });
      if (!foundNote) {
        res.status(400).send({ message: "note does not exist" });
      } else {
        const updatedNote = await foundNote.update({
          ...foundNote,
          name,
          subject,
          date,
          content,
          label
        }); //el face update pe toate atributele row-ului, punem operatorul ternal (...) pentru a specifica doar pe cine vr noi sa schimbam, daca nu l-am pune restul ar devenii null si nu vrem sa dam update cu null
        res.status(200).send({ updatedNote, message: "note has been updated" });
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.delete("/api/notes/:id", checkLogin, async (req, res) => {
    try {
      const { id } = req.params;
      const foundNote = await Note.findOne({
        where: {
          id,
          userId: req.session.id //id : id
        }
      });
      if (!foundNote) {
        res.status(400).send({ message: "note does not exist" });
      } else {
        await foundNote.destroy();
        res.status(200).send({ message: "note has been deleted" });
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  //-----------------------GROUPS

  app.get("/api/groups", checkLogin, async (req, res) => {
    try {
      const allGroups = [];

      const myGroups = await Group.findAll({
        where: { userId: req.session.id },
        raw: true
      });
      const sharedGroups = await Promise.all(
        (
          await UserGroup.findAll({
            where: { userId: req.session.id },
            raw: true
          })
        ).map(async usergroup => {
          return await Group.findOne({ where: { id: usergroup.id } });
        })
      );

      await Promise.all(
        myGroups.map(async group => {
          const owner = await User.findOne({
            attributes: ["id", "firstName", "lastName", "email"],
            where: { id: group.userId }
          });

          const usersIds = await UserGroup.findAll({
            where: { groupId: group.id },
            raw: true
          });

          const users = await Promise.all(
            usersIds.map(async user => {
              return await User.findOne({
                attributes: ["id", "firstName", "lastName", "email"],
                where: { id: user.userId }
              });
            })
          );
          users.push(owner);
          const notesIds = await GroupNotes.findAll({
            where: { groupId: group.id },
            raw: true
          });

          const notes = await Promise.all(
            notesIds.map(async note => {
              return await Note.findOne({
                where: { id: note.noteId }
              });
            })
          );

          allGroups.push({
            name: group.name,
            owner,
            users,
            notes,
            type: "mine"
          });
        })
      );

      await Promise.all(
        sharedGroups.map(async group => {
          const owner = await User.findOne({
            attributes: ["id", "firstName", "lastName", "email"],
            where: { id: group.userId }
          });

          const usersIds = await UserGroup.findAll({
            where: { groupId: group.id },
            raw: true
          });

          const users = await Promise.all(
            usersIds.map(async user => {
              return await User.findOne({
                attributes: ["id", "firstName", "lastName", "email"],
                where: { id: user.userId }
              });
            })
          );
          users.push(owner);
          const notesIds = await GroupNotes.findAll({
            where: { groupId: group.id },
            raw: true
          });
          const notes = await Promise.all(
            notesIds.map(async note => {
              return await Note.findOne({
                where: { id: note.noteId }
              });
            })
          );

          allGroups.push({
            name: group.name,
            owner,
            users,
            notes,
            type: "shared"
          });
        })
      );

      res.status(200).send(allGroups);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.post("/api/groups", checkLogin, async (req, res) => {
    try {
      const { name } = req.body;
      await Group.create({ name, userId: req.session.id });
      res.status(200).send({ message: "group inserted" });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });

  app.post("/api/groups/notes", checkLogin, async (req, res) => {
    try {
      const { noteId, groupId } = req.body;
      await GroupNotes.create({ noteId, groupId });
      res.status(200).send({ message: "note added to group" });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });
  app.post("/api/groups/users", checkLogin, async (req, res) => {
    try {
      const { userId, groupId } = req.body;
      await UserGroup.create({ userId, groupId });
      res.status(200).send({ message: "user added to group" });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "server error" });
    }
  });
};
module.exports = configure;

configure(app);

app.listen(port, () => {
  console.log("Serverul merge pe " + port);
});
