import Axios from "axios";

export async function setActivity({ commit }, activity) {
  commit("SET_ACTIVITY", activity);
}

export async function loadNotes({ commit }) {
  const response = await Axios.get("/api/notes");

  commit("SET_NOTES", response.data);
}

export async function setSelectedNote({ commit }, payload) {
  commit("SET_SELECTED_NOTE", payload);
}

export async function enableIsNew({ commit }) {
  commit("ENABLE_IS_NEW");
}

export async function disableIsNew({ commit }) {
  commit("DISABLE_IS_NEW");
}
