export function getIsNew(state) {
  return state.isNew;
}

export function getNotes(state) {
  return state.notes;
}

export function getSelectedNote(state) {
  return state.selectedNote;
}
