export function ENABLE_IS_NEW(state) {
  state.isNew = true;
}

export function DISABLE_IS_NEW(state) {
  state.isNew = false;
}

export function SET_NOTES(state, payload) {
  state.notes = payload;
}

export function SET_SELECTED_NOTE(state, payload) {
  state.selectedNote = payload;
}
