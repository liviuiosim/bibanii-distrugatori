const routes = [
	{
		path: '/',
		component: () => import('layouts/Layout.vue'),
		children: [
			{ path: '', component: () => import('pages/HomePage.vue') },
			{ path: '/profile', component: () => import('pages/ProfilePage.vue') },
			{ path: '/note', component: () => import('pages/NotePage.vue') }
		]
	},
	{
		path: '/login',
		component: () => import('layouts/AuthLayout.vue'),
		children: [ { path: '', component: () => import('pages/LoginPage.vue') } ]
	}
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
	routes.push({
		path: '*',
		component: () => import('pages/Error404.vue')
	});
}

export default routes;
